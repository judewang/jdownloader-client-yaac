export class RenameLink {
  linkId: number;
  newName: string;
}
