import { Priority } from '../models/priority.enum';

export class AddLink {
  assignJobID: boolean;
  autoExtract: boolean;
  autostart: boolean;
  dataURLs: string[];
  deepDecrypt: boolean;
  destinationFolder: string;
  downloadPassword: string;
  extractPassword: string;
  links: string | string[];
  overwritePackagizerRules: boolean;
  packageName: string;
  priority: Priority = Priority.DEFAULT;
  sourceUrl: string;
}
