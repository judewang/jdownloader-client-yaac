export interface DirectConnectionInfos {
  infos: DirectConnectionInfosServer[];
  rebindProtectionDetected: boolean;
  mode: string;
}

export interface DirectConnectionInfosServer {
  port: number;
  ip: string;
}
