import { Device } from './device.interface';

export interface RidResponse {
  rid: number;
}

export interface DataResponse<T> extends RidResponse {
  data: T;
}

export interface ConnectResponse extends RidResponse {
  sessiontoken: string;
  regaintoken: string;
}

export interface DevicesResponse extends RidResponse {
  list: Device[];
}
