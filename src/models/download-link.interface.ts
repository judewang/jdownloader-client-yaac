import { Priority } from './priority.enum';

export interface DownloadLink {
  addedDate: number;
  statusIconKey?: string;
  finished: boolean;
  packageUUID: number;
  uuid: number;
  speed?: number;
  url: string;
  enabled: boolean;
  bytesLoaded: number;
  host: string;
  name: string;
  bytesTotal: number;
  finishedDate?: number;
  status: string;
  skipped?: boolean;
  running?: boolean;
  extractionStatus?: string;
  priority?: Priority;
  eta?: number;
  downloadPassword?: string;
  comment?: string;
}
