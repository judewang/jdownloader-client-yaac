export enum Priority {
  HIGHEST = 'HIGHEST',
  HIGHER = 'HIGHER',
  HIGH = 'HIGH',
  DEFAULT = 'DEFAULT',
  LOW = 'LOW',
  LOWER = 'LOWER',
  LOWEST = 'LOWEST',
}
