import { DeviceStatus } from './device-status.enum';

export interface Device {
  id: string;
  type: 'jd';
  name: string;
  status: DeviceStatus;
}
