export enum SelectionType {
  SELECTED = 'SELECTED',
  UNSELECTED = 'UNSELECTED',
  ALL = 'ALL',
  NONE = 'NONE',
}
