import { JdownloaderClientCore } from './core';

describe('JdownloaderClient Core', () => {
  let api: JdownloaderClientCore;

  beforeEach(() => {
    api = new JdownloaderClientCore('', '');
  });

  it('should be defined', async () => {
    expect(api).toBeDefined();
  });
});
