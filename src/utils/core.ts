import { RidResponse } from '../models';

export class CoreUtils {
  static parse<T>(JSONstr: string) {
    return JSON.parse(JSONstr) as T;
  }

  static uniqueRid() {
    return Math.floor(Math.random() * 10e12);
  }

  static validateRid(data: RidResponse, rid: number) {
    if (data.rid !== rid) {
      throw new Error('RequestID mismatch');
    }
  }

  static errorParser(err: any): Error {
    if (err) {
      if (err.response) {
        if (err.response.data) {
          if (err.response.data.type) {
            return new Error(err.response.data.type);
          }
          return new Error(err.response.data);
        }
        return new Error(err.response);
      }
      return new Error(err);
    }
    return new Error('Unkown error');
  }
}
