import { JdownloaderClientCore } from '../core/core';
import { JdownloaderClientDevice } from './device';
import { DirectConnectionInfos } from '../models';

jest.mock('../core/core');

describe('JdownloaderClient Device', () => {
  let core: JdownloaderClientCore;
  let device: JdownloaderClientDevice;

  beforeEach(() => {
    core = new JdownloaderClientCore('', '');
    device = new JdownloaderClientDevice(core);
  });

  it('should get /getDirectConnectionInfos', async () => {
    const mockedResult: DirectConnectionInfos = {
      infos: [],
      rebindProtectionDetected: true,
      mode: 'test',
    };
    jest.spyOn(core, 'callAction').mockReturnValue(
      Promise.resolve({
        rid: 123,
        data: mockedResult,
      }),
    );

    const result = await device.getDirectConnectionInfos('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/device/getDirectConnectionInfos', 'deviceId');
    expect(result).toEqual(mockedResult);
  });
});
