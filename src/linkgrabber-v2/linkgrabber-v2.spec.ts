import { JdownloaderClientCore } from '../core/core';
import { JdownloaderClientLinkgrabberV2 } from './linkgrabber-v2';
import { AddLink, CrawledLinkQuery, RenameLink } from '../requests';
import { CrawledLink, Priority, AvailableLinkState } from '../models';

jest.mock('../core/core');

describe('JdownloaderClient LinkgrabberV2', () => {
  let core: JdownloaderClientCore;
  let linkGrabber: JdownloaderClientLinkgrabberV2;

  beforeEach(() => {
    core = new JdownloaderClientCore('', '');
    linkGrabber = new JdownloaderClientLinkgrabberV2(core);
  });

  it('should cleanup', async () => {
    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: null,
    });

    await linkGrabber.cleanup('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/linkgrabberv2/cleanup', 'deviceId', [
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
    ]);
  });

  it('should getPackageCount', async () => {
    const count = 123;
    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: count,
    });

    const result = await linkGrabber.getPackageCount('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/linkgrabberv2/getPackageCount', 'deviceId');
    expect(result).toEqual(count);
  });

  it('should queryLinks', async () => {
    const links: CrawledLink[] = [
      {
        availability: AvailableLinkState.ONLINE,
        bytesTotal: 1234,
        comment: 'comment',
        downloadPassword: 'password',
        enabled: true,
        host: 'AZ',
        name: 'mockedTest',
        packageUUID: 1234,
        priority: Priority.DEFAULT,
        url: 'url',
        uuid: 12345,
        variant: {
          iconKey: 'iconKey',
          id: 'id',
          name: 'name',
        },
        variants: true,
      },
    ];
    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: links,
    });

    const result = await linkGrabber.queryLinks('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/linkgrabberv2/queryLinks', 'deviceId', [
      JSON.stringify(new CrawledLinkQuery()),
    ]);
    expect(result).toEqual(links);
  });

  it('should addLinks - string', async () => {
    const newLinks = new AddLink();
    newLinks.autostart = true;
    newLinks.links = 'link1,link2';

    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: true,
    });

    await linkGrabber.addLinks('deviceId', newLinks);

    expect(core.callAction).toHaveBeenCalledWith('/linkgrabberv2/addLinks', 'deviceId', [JSON.stringify(newLinks)]);
  });

  it('should addLinks - array', async () => {
    const newLinks = new AddLink();
    newLinks.autostart = true;
    newLinks.links = ['link1', 'link2'];

    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: true,
    });

    await linkGrabber.addLinks('deviceId', newLinks);

    const mappedNewLinks = new AddLink();
    mappedNewLinks.autostart = true;
    mappedNewLinks.links = 'link1,link2';
    expect(core.callAction).toHaveBeenCalledWith('/linkgrabberv2/addLinks', 'deviceId', [
      JSON.stringify(mappedNewLinks),
    ]);
  });

  it('should /renameLink', async () => {
    const renameLink: RenameLink = {
      linkId: 123,
      newName: 'new name',
    };
    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: null,
    });

    await linkGrabber.renameLink('deviceId', renameLink);

    expect(core.callAction).toHaveBeenCalledWith('/linkgrabberv2/renameLink', 'deviceId', [
      renameLink.linkId,
      renameLink.newName,
    ]);
  });
});
