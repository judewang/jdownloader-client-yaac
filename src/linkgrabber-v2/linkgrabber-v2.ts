import { JdownloaderClientCore } from '../core/core';
import { CleanUp, AddLink, CrawledLinkQuery, RenameLink } from '../requests';
import { CrawledLink, LinkCollectingJob } from '../models';

export class JdownloaderClientLinkgrabberV2 {
  constructor(protected readonly core: JdownloaderClientCore) {}

  async addLinks(deviceId: string, addLink: AddLink): Promise<LinkCollectingJob> {
    const finalAdLlink: AddLink = Object.assign({}, addLink);
    if (Array.isArray(finalAdLlink.links)) {
      finalAdLlink.links = finalAdLlink.links.join(',');
    }
    const params = [JSON.stringify(finalAdLlink)];
    return (await this.core.callAction<LinkCollectingJob>('/linkgrabberv2/addLinks', deviceId, params)).data;
  }

  async queryLinks(
    deviceId: string,
    crawledLinkQuery: CrawledLinkQuery = new CrawledLinkQuery(),
  ): Promise<CrawledLink[]> {
    const params = [JSON.stringify(crawledLinkQuery)];
    return (await this.core.callAction<CrawledLink[]>('/linkgrabberv2/queryLinks', deviceId, params)).data;
  }

  async cleanup(deviceId: string, cleanup: CleanUp = new CleanUp()): Promise<void> {
    const params = [cleanup.linkIds, cleanup.packageIds, cleanup.action, cleanup.mode, cleanup.selectionType];
    await this.core.callAction('/linkgrabberv2/cleanup', deviceId, params);
  }

  async getPackageCount(deviceId: string): Promise<number> {
    return (await this.core.callAction<number>('/linkgrabberv2/getPackageCount', deviceId)).data;
  }

  async renameLink(deviceId: string, renameLink: RenameLink): Promise<void> {
    const params = [renameLink.linkId, renameLink.newName];
    await this.core.callAction<void>('/linkgrabberv2/renameLink', deviceId, params);
  }
}
