import { JdownloaderClientCore } from '../core/core';
import { ForceDownload } from '../requests';

export class JdownloaderClientDownloadController {
  constructor(protected readonly core: JdownloaderClientCore) {}

  async forceDownload(deviceId: string, forceDownload: ForceDownload = new ForceDownload()): Promise<void> {
    const params = [forceDownload.linkIds, forceDownload.packageIds];
    await this.core.callAction('/downloadcontroller/forceDownload', deviceId, params);
  }

  async getCurrentState(deviceId: string): Promise<string> {
    return (await this.core.callAction<string>('/downloadcontroller/getCurrentState', deviceId)).data;
  }

  async getSpeedInBps(deviceId: string): Promise<number> {
    return (await this.core.callAction<number>('/downloadcontroller/getSpeedInBps', deviceId)).data;
  }

  async pause(deviceId: string, value: boolean = true): Promise<void> {
    await this.core.callAction('/downloadcontroller/pause', deviceId, [value]);
  }

  async start(deviceId: string): Promise<void> {
    await this.core.callAction('/downloadcontroller/start', deviceId);
  }

  async stop(deviceId: string): Promise<void> {
    await this.core.callAction('/downloadcontroller/stop', deviceId);
  }
}
