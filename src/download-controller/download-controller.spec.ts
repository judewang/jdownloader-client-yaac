import { JdownloaderClientCore } from '../core/core';
import { JdownloaderClientDownloadController } from './download-controller';

jest.mock('../core/core');

describe('JdownloaderClient DownloadController', () => {
  let core: JdownloaderClientCore;
  let downloadcontroller: JdownloaderClientDownloadController;

  beforeEach(() => {
    core = new JdownloaderClientCore('', '');
    downloadcontroller = new JdownloaderClientDownloadController(core);
  });

  it('should /forceDownload', async () => {
    jest.spyOn(core, 'callAction').mockReturnValue(
      Promise.resolve({
        rid: 123,
        data: null,
      }),
    );

    await downloadcontroller.forceDownload('deviceId');
    expect(core.callAction).toHaveBeenCalledWith('/downloadcontroller/forceDownload', 'deviceId', [
      undefined,
      undefined,
    ]);
  });

  it('should /getCurrentState', async () => {
    const state = 'mockedState';
    jest.spyOn(core, 'callAction').mockReturnValue(
      Promise.resolve({
        rid: 123,
        data: state,
      }),
    );

    const result = await downloadcontroller.getCurrentState('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/downloadcontroller/getCurrentState', 'deviceId');
    expect(result).toEqual(state);
  });

  it('should /getSpeedInBps', async () => {
    const currentSpeed = 1234;
    jest.spyOn(core, 'callAction').mockReturnValue(
      Promise.resolve({
        rid: 123,
        data: currentSpeed,
      }),
    );

    const result = await downloadcontroller.getSpeedInBps('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/downloadcontroller/getSpeedInBps', 'deviceId');
    expect(result).toEqual(currentSpeed);
  });

  it('should /pause', async () => {
    jest.spyOn(core, 'callAction').mockReturnValue(
      Promise.resolve({
        rid: 123,
        data: null,
      }),
    );

    await downloadcontroller.pause('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/downloadcontroller/pause', 'deviceId', [true]);
  });

  it('should /start', async () => {
    jest.spyOn(core, 'callAction').mockReturnValue(
      Promise.resolve({
        rid: 123,
        data: null,
      }),
    );

    await downloadcontroller.start('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/downloadcontroller/start', 'deviceId');
  });

  it('should /stop', async () => {
    jest.spyOn(core, 'callAction').mockReturnValue(
      Promise.resolve({
        rid: 123,
        data: null,
      }),
    );

    await downloadcontroller.stop('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/downloadcontroller/stop', 'deviceId');
  });
});
