# jdownloader-client-YAAC ( Yet Another Api Client)

Node.js JDownloader client, written in TypeScript based in [jdownloader-api](https://www.npmjs.com/package/jdownloader-api) and [jdownloader-client](https://www.npmjs.com/package/jdownloader-client).

## Installation

```
npm i --save jdownloader-client-yaac
```

## Usage

```javascript
const { JdownloaderClient } = require('jdownloader-client-yaac')

async function () {
  const client = new JdownloaderClient('<email>', '<password>');
  await client.core.connect();
  const devices = await client.core.listDevices();
  const downloadLinks = await client.downloadsV2.queryLinks(devices[0].id);
  console.log(downloadLinks)
}

main()
  .then(() => console.log('Done'))
  .catch(err => console.error(err))
```

## Implemented methods

* [`/device`](https://my.jdownloader.org/developers/#tag_79)
   * [`/getDirectConnectionInfos`](https://my.jdownloader.org/developers/#tag_80)
* [`/downloadcontroller`](https://my.jdownloader.org/developers/#tag_90)
   * [`/forceDownload`](https://my.jdownloader.org/developers/#tag_91)
   * [`/getCurrentState`](https://my.jdownloader.org/developers/#tag_92)
   * [`/getSpeedInBps`](https://my.jdownloader.org/developers/#tag_93)
   * [`/pause`](https://my.jdownloader.org/developers/#tag_94)
   * [`/start`](https://my.jdownloader.org/developers/#tag_95)
   * [`/stop`](https://my.jdownloader.org/developers/#tag_96)
* [`/downloadsV2`](https://my.jdownloader.org/developers/#tag_127)
   * [`/cleanup`](https://my.jdownloader.org/developers/#tag_131)
   * [`/packageCount`](https://my.jdownloader.org/developers/#tag_142)
   * [`/queryLinks`](https://my.jdownloader.org/developers/#tag_143)
   * [`/queryPackages`](https://my.jdownloader.org/developers/#tag_146)
* [`/linkgrabberv2`](https://my.jdownloader.org/developers/#tag_239)
   * [`/addLinks`](https://my.jdownloader.org/developers/#tag_245)
   * [`/queryLinks`](https://my.jdownloader.org/developers/#tag_267)
   * [`/cleanup`](https://my.jdownloader.org/developers/#tag_247)
   * [`getPackageCount`](https://my.jdownloader.org/developers/#tag_252)
